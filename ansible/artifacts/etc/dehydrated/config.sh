##########################
# Dehydrated config file #
##########################

# Path to certificate authority (default: https://acme-v02.api.letsencrypt.org/directory)
CA="https://acme-v02.api.letsencrypt.org/directory"
#CA="https://acme-staging.api.letsencrypt.org/directory"


# Path to old certificate authority
# Set this value to your old CA value when upgrading from ACMEv1 to ACMEv2 under a different endpoint.
# If dehydrated detects an account-key for the old CA it will automatically reuse that key
# instead of registering a new one.
# default: https://acme-v01.api.letsencrypt.org/directory
#OLDCA="https://acme-v01.api.letsencrypt.org/directory"

CHALLENGETYPE="http-01"

# Automatic cleanup (default: no)
AUTO_CLEANUP="yes"

# Email for registration
CONTACT_EMAIL="adam.jones@codethink.co.uk"
