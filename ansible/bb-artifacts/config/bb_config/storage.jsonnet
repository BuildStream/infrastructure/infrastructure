{
  blobstore: {
    contentAddressableStorage: {
      circular: {
        directory: '/cas',
        offsetFileSizeBytes: 16 * 1024 * 1024,
        offsetCacheSize: 10000,
        dataFileSizeBytes: 100 * 1024 * 1024 * 1024,
        dataAllocationChunkSizeBytes: 16 * 1024 * 1024,
      },
    },
    actionCache: {
      circular: {
        directory: '/ac',
        offsetFileSizeBytes: 1024 * 1024,
        offsetCacheSize: 1000,
        dataFileSizeBytes: 100 * 1024 * 1024,
        dataAllocationChunkSizeBytes: 1048576,
        instances: [''],
      },
    },
  },
  httpListenAddress: ':6981',
  grpcServers: [{
    listenAddresses: [':7982'],
    authenticationPolicy: { allow: {} },
  }],
  allowAcUpdatesForInstanceNamePrefixes: [''],
  maximumMessageSizeBytes: 16 * 1024 * 1024,
}
