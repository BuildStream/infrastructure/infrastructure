{
  // Simple Caching Fetcher, only returns Push'd content
  fetcher: {
    caching: {
      fetcher: {
        'error': {
          code: 5,
          message: "Asset Not Found",
        }
      }
    }
  },

  // On disk storage for references
  assetStore: {
    circular: {
      directory: '/storage',
      offsetFileSizeBytes: 1024 * 1024,
      offsetCacheSize: 1000,
      dataFileSizeBytes: 100 * 1024 * 1024,
      dataAllocationChunkSizeBytes: 1048576,
      instances: [''],
    },
  },
  httpListenAddress: ':1111',
  grpcServers: [{
    listenAddresses: [':7981'],
    authenticationPolicy: { allow: {} },
  }],
  allowUpdatesForInstances: [''],
  maximumMessageSizeBytes: 16 * 1024 * 1024,
}

