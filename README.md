# Destroying old droplets

This repo includes a Python script "destroy_old_droplets.py" which destroys
*runner* droplets (machines spun up to run jobs) which are more than one day
old.

The script requires the external Python package
[python-digitalocean](https://github.com/koalalorenzo/python-digitalocean)
(which can be installed via pip) - this is a Python wrapper around DigitalOcean's
API. I would recommend installing it into a virtualenv.

In order for the library to access the server, you need a Token. This can be
obtained via Digital Ocean's web UI: Manage -> API -> Generate New Token.

## The cronjob
This script is currently running on an hourly cronjob (on the bastion):

    @hourly /root/destroy_droplets/destroy_droplets.sh >/dev/null 2>&1

Where *destroy_droplets.sh* is just a shell script which activates the
virtual environment, runs the python script and then deactivates the venv.

* To view cronjobs on the bastion: `crontab -l`.
* To edit cronjobs: `crontab -e`

## The Ansible Scripts
This repo holds Ansible playbooks for setting up some of the Buildstream
 project infrastructure.

To configure a new CAS server, It's used as follows:

```
ansible-playbook  \
    -i hosts ./bb-artifacts/main.yml \
    -e 'ansible_python_interpreter=/usr/bin/python3 ansible_user=root' \
    --vault-password-file=password-file.txt \
    --tags "test-artifacts"
```

The Vault password is currently controlled by tcanabrava, this is needed to decrypt the certificate files
