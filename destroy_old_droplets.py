#!/usr/bin/env python3
import datetime

import digitalocean  # 3rd party library (python-digitalocean), installed into venv

# Token generated on DigitalOcean's web API
manager = digitalocean.Manager(token="TOKEN")
my_droplets = manager.get_all_droplets()

now = datetime.datetime.now()
for droplet in my_droplets:
	# Create a datetime object representing when the droplet was created
	created = datetime.datetime.strptime(droplet.created_at, "%Y-%m-%dT%H:%M:%SZ")
	delta = now - created

	# If the droplet is older than one day, destroy it
        # If the droplet is older than 3 hours and is not an overnight droplet, destroy it
	# Only destroy droplets that are runners
	if droplet.name.startswith("runner") and delta.days > 0:
		droplet.destroy()
